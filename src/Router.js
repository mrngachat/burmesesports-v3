import React,{Component} from 'react'
import {Text} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { createSwitchNavigator, createStackNavigator, createAppContainer, StackNavigator, createBottomTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import { FluidNavigator } from 'react-navigation-fluid-transitions'
import { fadeIn, fadeOut, zoomIn, zoomOut } from 'react-navigation-transitions';

import Home from './screens/Home'
import Movies from './screens/Movies'
import Detail from './screens/Detail'
import MyVideoPlayer from './screens/VideoPlayer'
import Player from './screens/Player'

const Details = FluidNavigator({
    Detail: Detail   
})



const MovieStack = FluidNavigator({
    Movies : {
        screen : Movies,
        navigationOptions:{
            header:null
        }
    }
})


const HomeStack = createStackNavigator({
    Home : Home,
    Detail :  {
        screen: Details, navigationOptions: {
            title: "Burmese Sports",
            headerTintColor: '#ffffff',
            headerStyle: { backgroundColor: '#0750d6', height: 65 },
        }},
        MyVideoPlayer : {
            screen : MyVideoPlayer, navigationOptions : {
                header : null
            }
        }
},{
    initialRouteName: 'Home',
    transitionConfig: () => zoomIn(),
  },)



HomeStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = true;
    let { routeName } = navigation.state.routes[navigation.state.index];
    if (routeName === 'Detail' || routeName === 'MyVideoPlayer') {
        tabBarVisible = false;
     }
     return {
        tabBarVisible,
     };
}

const text = <Text>Hello</Text>

const App = createBottomTabNavigator({
    Home : {
        screen : HomeStack,
        navigationOptions: {
            tabBarIcon: ({ tintColor, focused }) => (
                <Icon name="home" color={tintColor}
                    size={25}
                />
            ),
            tabBarLabel:({tintColor})=> (
                <Text style={{textAlign:'center',fontFamily:'pyidaungsu',color:tintColor}} >ပွဲစဉ်များ</Text>
            )

        },
    },
    About : {
        screen : MovieStack,
        navigationOptions: {
            tabBarIcon: ({ tintColor, focused }) => (
                <Icon name="info" color={tintColor}
                    size={25}
                />
            ),
            tabBarLabel:({tintColor})=> (
                <Text style={{textAlign:'center',fontFamily:'pyidaungsu',color:tintColor}} >အကြောင်းအရာ</Text>
            )
        
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
        showIcon: true,
        style: { backgroundColor: '#0750d6', height: 68 },
        tabStyle: { marginBottom: 10 }
    }})

export default createAppContainer(App)


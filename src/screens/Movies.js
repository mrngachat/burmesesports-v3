import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, Linking, ImageBackground, Dimensions } from 'react-native';
// import {
//     AdMobBanner,
//     AdMobRewarded,
//     AdMobInterstitial
// } from 'react-native-admob';
import { Transition } from 'react-navigation-fluid-transitions'

const { width } = Dimensions.get('screen')
class Movies extends Component {
    static navigationOptions = ({ navigation, screenProps }) => ({
        title: "About",
        headerTintColor: 'white',
        headerStyle: { backgroundColor: '#0750d6', height: 65, },
        headerTitleStyle: { textAlign: 'center', flex: 1, fontWeight: 'bold' },
    });

    openFB = () => {
        Linking.openURL("fb://profile=mrngachat");
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center' }} >


                <ImageBackground resizeMode='stretch'
                    source={require('./../../image/bg.png')} style={{ width: width, height: 500 }} >
                    <Transition appear='scale' >
                        <View style={{ marginTop: 40, alignItems: 'center' }} >
                            <Image
                                style={{ width: 150, height: 150 }}
                                source={require('./../../image/logo.png')}
                            />
                            <Text style={{ textAlign: 'center',fontFamily:'pyidaungsu', marginTop: 19, fontSize: 20 }} >ဗားရှင်း - ၁.၃ (စမ်းသပ် ဗားရှင်း)</Text>
                        </View>
                    </Transition>

                    <View style={{ marginTop: 50 }} >
                        <Transition appear='right' disappear='left'>
                            <View style={styles.bar} ><Text style={{ color: 'black', fontSize: 18, fontFamily:'pyidaungsu' }} >ဆော့ဖ်ဝဲလ် ရေးသားသူ - ငချက်</Text></View>
                        </Transition>

                        <Transition appear='left' disappear='right'>
                            <View style={styles.bar} ><Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold' }} >mrngachat@gmail.com</Text></View>
                        </Transition>

                        {/* <TouchableOpacity style={styles.bar} onPress={() => this.openFB()}  ><Text>Facebook Profile</Text></TouchableOpacity> */}
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bar: {
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        padding: 15,
        marginTop: 10,
        paddingHorizontal: 40,
        alignItems: 'center'
    }
});
export default Movies
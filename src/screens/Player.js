import React, { Component } from 'react';
import { BackHandler, View, StatusBar, StyleSheet, Dimensions } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation'
import Video from 'react-native-video';
import { WebView } from 'react-native-webview';

const { width } = Dimensions.get('screen')
var styles = StyleSheet.create({
    backgroundVideo: {

        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        width: width,
        flex: 1
    },
});


class Player extends Component {
    constructor() {
        super();
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);



    }


    componentDidMount() {
        // Orientation.lockToLandscape();

    }

    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);


    }

    handleBackButtonClick() {
        Orientation.lockToPortrait()
        this.props.navigation.goBack()

        return true;
    }

    onBack() {
        Orientation.lockToPortrait()
        this.props.navigation.goBack()

        return true;
    }




    render() {
        var { params } = this.props.navigation.state;
        const html = `
        <iframe width='640' height='360' marginwidth='0' marginheight='0' scrolling='no' frameborder='0' allowfullscreen='yes' src='https://box-live.stream/stream/52367.html'></iframe>
           `;
        var { params } = this.props.navigation.state;

        return (
            <View style={{ flex: 1 }} >
                <StatusBar hidden />
                {/* <VideoPlayer
                    source={{ uri:params.item.link}}
                    navigator={this.props.navigation}
                    autoplay={false}
                /> */}
                
                <WebView
                    source={{ html }}
          
                /> 
            </View>
        );
    }
}

Player.defaultProps = {};

Player.propTypes = {};

export default Player;
import React, { Component } from 'react';
import { View, Image, Dimensions, TouchableOpacity, Alert } from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions'
import axios from 'axios';
import { Container, Header, Content, Button, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    AdMobBanner,
    AdMobRewarded,
    AdMobInterstitial
} from 'react-native-admob';
const { width } = Dimensions.get('screen')
class Detail extends Component {
    constructor() {
        super();

        this.state = {
            data: null,
            status: false
        };


    }



    componentDidMount() {
        setTimeout(() => this.interShow(), 1000);



        AdMobInterstitial.setAdUnitID('ca-app-pub-9378050611043280/7417871508');

        AdMobInterstitial.addEventListener('adLoaded',
            () => console.log('AdMobInterstitial adLoaded')
        );
        AdMobInterstitial.addEventListener('adFailedToLoad',
            (error) => console.warn(error)
        );
        AdMobInterstitial.addEventListener('adOpened',
            () => console.log('AdMobInterstitial => adOpened')
        );
        AdMobInterstitial.addEventListener('adClosed',
            () => {
                console.log('AdMobInterstitial => adClosed');
                AdMobInterstitial.requestAd().catch(error => console.warn(error));
            }
        );
        AdMobInterstitial.addEventListener('adLeftApplication',
            () => console.log('AdMobInterstitial => adLeftApplication')
        );

        AdMobInterstitial.requestAd().catch(error => console.warn(error));




     
    }

    componentWillUnmount() {
        
        AdMobInterstitial.removeAllListeners();
    }

    interShow = () => {
        AdMobInterstitial.showAd().catch(error => console.warn(error));
    }

   

    toPlay = (item) => {
        if (item.status) {
            this.props.navigation.navigate('MyVideoPlayer', { item })
        } else {
            this.setState({ status: true })
        }
    }

    render() {

        // const { data } = this.state
        const { item } = this.props.navigation.state.params
        const { status } = this.state

        return (
            <View style={{ flex: 1, alignItems: "center" }}>

                <View>


                    <View>
                        <Image
                            style={{ width: width, height: 190 }}
                            source={{ uri: item.image }}
                        />
                    </View>





                    <View style={{
                        width: 100,
                        height: 100,
                        borderRadius: 100 / 2,
                        backgroundColor: 'white',
                        position: 'absolute',
                        bottom: -50,
                        alignSelf: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.5,
                        shadowRadius: 2,
                        elevation: 5,

                    }} >

                        <TouchableOpacity onPress={() => {this.toPlay(item)}} >
                            <Icon name="play-circle-o" size={80} color="#900" />
                        </TouchableOpacity>
                    </View>

                </View>

                {status &&
                    <View style={{ marginTop: 60 }} >
                        <Text style={{ color: 'red', fontFamily: 'pyidaungsu' }} >ကစားပွဲ မစသေးပါ !</Text>
                    </View>}
                <View style={{ marginTop: 50 }} >

                    <Text style={{ fontSize: 35, fontFamily: 'pyidaungsu', marginTop: 10, textAlign: 'center',  }} >{item.title}</Text>

                </View>

                <View style={{ alignItems: 'center', marginTop: 40 }} >
                    <AdMobBanner
                        adSize="banner"
                        adUnitID="ca-app-pub-9378050611043280/2357116515"
                        ref={el => (this._basicExample = el)}
                    />
                </View>


                <View style={{ justifyContent: 'flex-end', flex: 1 }} >

                    <View style={{ alignItems: 'center' }} >
                        <AdMobBanner
                            adSize="banner"
                            adUnitID="ca-app-pub-9378050611043280/7034728122"
                            ref={el => (this._basicExample = el)}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

export default Detail
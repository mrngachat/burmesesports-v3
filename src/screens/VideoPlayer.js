import React, { Component } from 'react';
import { BackHandler, View, StatusBar, StyleSheet,Dimensions } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation'
import Video from 'react-native-video';
const { width } = Dimensions.get('screen')
var styles = StyleSheet.create({
    backgroundVideo: {
     
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      width:width,
      flex:1
    },
  });
  
class MyVideoPlayer extends Component {
    constructor() {
        super();
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);



    }


    componentDidMount() {
        Orientation.lockToLandscape();

    }

    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);


    }

    handleBackButtonClick() {
        Orientation.lockToPortrait()
        this.props.navigation.goBack()

        return true;
    }

    onBack() {
        Orientation.lockToPortrait()
        this.props.navigation.goBack()

        return true;
    }




    render() {
        var { params } = this.props.navigation.state;
        console.warn(params.item.link)
        return (
            <View style={{ flex: 1 }} >
                <StatusBar hidden />
                <VideoPlayer
                    source={{ uri:params.item.link}}
                    navigator={this.props.navigation}
                />
                {/* <Video source={{ uri:params.item.link }}   // Can be a URL or a local file.
                    ref={(ref) => {
                        this.player = ref
                    }}                                      // Store reference
                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                    onError={this.videoError} 
                                  // Callback when video cannot be loaded
                    style={styles.backgroundVideo} /> */}
            </View>
        );
    }
}

MyVideoPlayer.defaultProps = {};

MyVideoPlayer.propTypes = {};

export default MyVideoPlayer;
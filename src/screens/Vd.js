import React, { Component } from 'react';
import { BackHandler, View, StatusBar } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation'

class MyVideoPlayer extends Component {
    constructor() {
        super();
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);



    }


    componentDidMount() {
        Orientation.lockToLandscape();
       
      }
  
      componentWillUnmount() {
       
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
      }

      componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        

    }

    handleBackButtonClick() {
        Orientation.lockToPortrait()
        this.props.navigation.goBack()
        
        return true;
    }

    onBack(){
        Orientation.lockToPortrait()
        this.props.navigation.goBack()
        
        return true;
    }




    render() {
        var { params } = this.props.navigation.state;
        console.warn(params.item.link)
        return (
            <View style={{ flex: 1 }} >
                <StatusBar hidden />
                <VideoPlayer
                    source={{ uri:params.item.link}}
                    navigator={this.props.navigation}
                />
            </View>
        );
    }
}

MyVideoPlayer.defaultProps = {};

MyVideoPlayer.propTypes = {};

export default MyVideoPlayer;
import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, StatusBar, Image, Dimensions, ScrollView, ImageBackground, Alert, BackHandler, Linking,TouchableOpacity } from 'react-native';
import { Container, Header, Content, Button } from 'native-base';
import axios from 'axios';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
} from 'react-native-indicators';
import {
    AdMobBanner,
    AdMobRewarded,
    AdMobInterstitial
} from 'react-native-admob';

import Orientation from 'react-native-orientation'

const { width } = Dimensions.get('screen')
class Home extends Component {
    constructor() {
        super();

        this.state = {
            data: [],
            loading: true,
            nodata: false,
            maintain: false,
            update: false,
            url:''
        };


    }

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null
    });

    componentDidMount() {
        setTimeout(() => this.showRewarded(), 1000);
        Orientation.lockToPortrait();
        axios.get('https://api.balllone.online/versioncontrol').then(res => {
            let check = res.data
            this.setState({ loading: false })
            if (check.version > 3) {
                this.setState({ update: true, url : check.url })
               

            } else if (check.server) {
                this.setState({ maintain: true })
            } else {
                axios.get('https://api.balllone.online',{params:{package:'com.burmesesports'}}).then(response => {
                    if (response.data.length == 0) {
                        this.setState({ nodata: true })
                    }
                    this.setState({ data: response.data, loading: false })
                })
            }
        })



        // axios.get('https://api.balllone.online').then(response => this.setState({ data: response.data, loading: false }))
        AdMobRewarded.setAdUnitID('ca-app-pub-9378050611043280/4791708165');

        AdMobRewarded.addEventListener('rewarded',
            (reward) => console.log('AdMobRewarded => rewarded', reward)
        );
        AdMobRewarded.addEventListener('adLoaded',
            () => console.log('AdMobRewarded => adLoaded')
        );
        AdMobRewarded.addEventListener('adFailedToLoad',
            (error) => console.warn(error)
        );
        AdMobRewarded.addEventListener('adOpened',
            () => console.log('AdMobRewarded => adOpened')
        );
        AdMobRewarded.addEventListener('videoStarted',
            () => console.log('AdMobRewarded => videoStarted')
        );
        AdMobRewarded.addEventListener('adClosed',
            () => {
                console.log('AdMobRewarded => adClosed');
                AdMobRewarded.requestAd().catch(error => console.warn(error));
            }
        );
        AdMobRewarded.addEventListener('adLeftApplication',
            () => console.log('AdMobRewarded => adLeftApplication')
        );

        AdMobRewarded.requestAd().catch(error => console.warn(error));
    }

    

    toDetail = (item) => {

        this.props.navigation.push('Detail', { item })

    }

    showRewarded = () => {
        AdMobRewarded.showAd().catch(error => console.warn(error));
    }

    updateApp = () => {
        const {url} = this.state
        Linking.openURL(url).catch((err) => console.warn('An error occurred', err));
    }
    navigate = (item) => {
        this.props.navigation.push('Detail', { item })
    }


    render() {
        let { data, loading, nodata, maintain, update } = this.state

        return (
            <View style={{ flex: 1 }} >
                <StatusBar
                    backgroundColor="#0750d6"
                    barStyle="light-content"
                />
                <ImageBackground resizeMode='cover'
                    source={require('./../../image/bg.png')} style={{ width: width, flex: 1 }} >
                    <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ fontSize: 30, fontWeight: 'bold', color: 'white' }} >Burmese Sports</Text>
                    </View>
                    {update &&
                        <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }} >
                            <Text style={{ fontFamily: 'pyidaungsu', fontSize: 22, color: 'red' }} >ဗားရှင်း အသစ် </Text>
                            <TouchableOpacity onPress={this.updateApp} >
                                <Text style={{
                                    color: 'white',
                                    fontFamily: 'pyidaungsu',
                                    fontSize: 20,
                                    paddingHorizontal: 25,
                                    paddingVertical: 10,
                                    backgroundColor: 'red',
                                    marginTop: 20

                                }} >ဒေါင်းလုတ် ဆွဲရန် နှိပ်ပါ</Text>
                            </TouchableOpacity>
                        </View>
                    }

                    {maintain &&
                        <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }} >
                            <Text style={{
                                color: 'white',
                                fontFamily: 'pyidaungsu',
                                fontSize: 20,
                                paddingHorizontal: 25,
                                paddingVertical: 10,
                                backgroundColor: 'red'

                            }} >ဆာဗာပြုပြင်နေပါသည် !</Text>
                        </View>
                    }
                    {nodata &&
                        <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }} >
                            <Text style={{
                                color: 'white',
                                fontFamily: 'pyidaungsu',
                                fontSize: 20,
                                paddingHorizontal: 25,
                                paddingVertical: 10,
                                backgroundColor: 'red'

                            }} >ပွဲစဉ်မရှိသေးပါ</Text>
                        </View>
                    }
                    {loading &&

                        <View style={{ flex: 1 }} >
                            <SkypeIndicator color='red' />
                        </View>

                    }

                    {data.length > 0 &&
                        <View style={{ flex: 1 }} >
                            <ScrollView style={{ bottom: 0 }} >
                                {data.map((item, i) => (
                                    <View style={{
                                        height: 230, marginTop: 10, marginHorizontal: 10, flex: 1, backgroundColor: 'white', borderRadius: 15,
                                        shadowColor: '#000',
                                        shadowOffset: { width: 0, height: 2 },
                                        shadowOpacity: 0.5,
                                        shadowRadius: 2,
                                        elevation: 5,
                                    }} >
                                        <View style={{ flex: 1, }} >
                                            <Image
                                                style={{ marginVertical: 12, flex: 1 }}
                                                source={{ uri: item.image }}
                                            />
                                            <View style={{
                                                position: 'absolute',
                                                alignSelf: 'center',
                                                justifyContent: 'space-around',
                                                width: 100,
                                                height: 40,
                                                backgroundColor: 'red', marginTop: 10,
                                                borderRadius: 10
                                            }} ><Text style={{ textAlign: 'center', color: 'white', fontWeight: '900' }} >{item.time}</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 10, marginBottom: 10 }} >
                                            <View style={{ width: 200 }} >
                                                <Text numberOfLines={2} style={{ color: 'black', fontFamily: 'pyidaungsu' }} >{item.title}</Text>
                                            </View>
                                            <View>
                                                <Button style={{ width: 90, justifyContent: 'center', alignItems: 'center', borderRadius: 8 }} onPress={() => this.toDetail(item)} >
                                                    <Text style={{ color: 'white', fontFamily: 'pyidaungsu' }} >ကြည့်ရှုရန်</Text>
                                                </Button>
                                            </View>
                                        </View>
                                    </View>
                                ))}

                            </ScrollView>
                        </View>
                    }



                    {!loading &&
                        <View style={{ alignItems: 'center' }} >
                            <AdMobBanner
                                adSize="banner"
                                adUnitID="ca-app-pub-9378050611043280/2357116515"
                                ref={el => (this._basicExample = el)}
                            />
                        </View>}




                </ImageBackground>







            </View>
        )
    }
}


export default Home